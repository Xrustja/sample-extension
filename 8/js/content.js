init();

async function init() {
    let price = await elementAppear("#priceblock_dealprice");
    if (!price) {
        price = await elementAppear("#priceblock_ourprice")
    }
    if (price) {
        let result = await requestBackground(new ExtensionMessage(config.keys.requestBackground, {message: price.innerText}))
        price.innerText = '$' + result.message;
    }
}

// chrome.runtime.sendMessage(new ExtensionMessage(config.keys.getBackgroundMessage, {message: price.innerText}))
//
// chrome.runtime.onMessage.addListener(function(message,sender,sendResponse){
//     switch(message.context){
//         case config.keys.getContentMessage:
//             console.log("get message from  BG ", message.data);
//             break;
//     }
// });
