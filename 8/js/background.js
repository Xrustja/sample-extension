chrome.runtime.onMessage.addListener(
    function(message, sender){
        executeFunction(message.context, {message, sender});
    }
);

// var extensionValue = "Test value";
//
// async function getBackgroundInfo({message, sender}){
//     //For popup response
//     // chrome.extension.sendMessage(new ExtensionMessage(message.context, {extensionValue}));
// console.log("BG message => ", message, "BG sender => ", sender)
//     //for Content script response
//     sendPageMessage(new ExtensionMessage(message.context, {extensionValue}), sender.tab.id);
// };
//
// async function getBackgroundMessage({message, sender}){
//     console.log(message.data)
//     chrome.extension.sendMessage(new ExtensionMessage(config.keys.getPopupMessage, {message: "Data to popup"}))
// };



async function requestBackground({message, sender}){
    let price = message.data.message.slice(1);
    let newPrice = (price/2).toFixed(2);
    sendPageMessage(new ExtensionMessage(message.context, {message: newPrice}), sender.tab.id)
};
  