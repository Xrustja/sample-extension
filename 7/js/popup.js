
init()

async function init() {
    setTimeout(() => {
        const loader = document.querySelector(".loader");
        loader.className += " hidden";
        const form = document.querySelector(".login-form");
        form.className += " show";
    }, 1000);


    document.querySelector('form').addEventListener('submit', (e) => {
            e.preventDefault();
            let error1 = document.querySelectorAll('.error')[0];
            let error2 = document.querySelectorAll('.error')[1];

            if (e.target[0].value === "") {
                error1.classList.add("show")
            } else {
                if (error1.classList.contains('show')) {
                    error1.classList.remove('show')
                }
            }
            if (e.target[1].value === "") {
                error2.classList.add("show");
            } else {
                if (error2.classList.contains('show')) {
                    error2.classList.remove('show')
                }
            }
        }
    )
    ;

}
