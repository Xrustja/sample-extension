init();

async function init(){
    let price = await elementAppear("#priceblock_dealprice");
    if (!price) {
        price = await elementAppear("#priceblock_ourprice")
    }
    if (price) {
        let result = await requestBackground(new ExtensionMessage(config.keys.requestBackground, {message: price.innerText}))
        price.innerText = '£' + result.message;
    }
}

