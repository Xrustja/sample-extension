chrome.runtime.onMessage.addListener(
    function(message, sender){
        executeFunction(message.context, {message, sender});
    }
);

async function requestBackground({message, sender}){
    let price = message.data.message.slice(1);
    fetch("https://api.coindesk.com/v1/bpi/currentprice.json")
        .then(async (res) => res.json())
        .then(data => {
            let usd =data.bpi.USD.rate.replace(",",'');
            let gbp = data.bpi.GBP.rate.replace(",",'');
            let newPrice = (price / usd * gbp).toFixed(2);
            sendPageMessage(new ExtensionMessage(message.context, {message: newPrice}), sender.tab.id)
        });
};


init()

async function init(){
    //
    // $.ajax({
    //     type: "GET",
    //     url: "https://api.coindesk.com/v1/bpi/currentprice.json",
    //     dataType: "json",
    //     success: result => {
    //         console.log(result);
    //     },
    //     error: result => {
    //         console.log(result);
    //     },
    // });
}