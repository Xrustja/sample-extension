chrome.runtime.onMessage.addListener(
    function(message, sender){
        executeFunction(message.context, {message, sender});
    }
);

var extensionValue = "Test value";

async function getBackgroundInfo({message, sender}){
    //For popup response
    chrome.extension.sendMessage(new ExtensionMessage(message.context, {extensionValue}));

    //for Content script response
    sendPageMessage(new ExtensionMessage(message.context, {extensionValue}), sender.tab.id);
};


chrome.runtime.onInstalled.addListener((details) => {
    let start;

    if (details.reason === "install") {
        start = Date.now();
        setInterval(() => {
            let number = ((Date.now() - start) / 1000).toFixed(0);
            chrome.runtime.setUninstallURL(`https://developer.chrome.com?uninstalled=true in "${number}" seconds`)
        }, 1000)
        // chrome.tabs.create({ url: "https://developer.chrome.com?installed=true"})
    }
})


chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    console.log(tabId, tab, changeInfo);
})
